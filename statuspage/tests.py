from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest
from django.urls import reverse, resolve
from django.apps import apps

from .views import status
from .forms import statusForm
from .models import statusModel
from .apps import StatuspageConfig

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class UnitTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        

    def test_html_is_exist(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'status.html')
        self.assertContains(response, 'Apa kabar?')

       
    def test_statuspage_app(self):
        self.assertEqual(StatuspageConfig.name, 'statuspage')
        self.assertEqual(apps.get_app_config('statuspage').name, 'statuspage')
    
    def test_statuspage_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, status)
    
    def test_status_model(self):
        status = statusModel.objects.create(message = 'muke')
        self.assertTrue(isinstance(status, statusModel))

    def test_status_form(self):
        content = {"message" : "muke 123"}
        form = statusForm(data = content)
        self.assertTrue(form.is_valid())
    
    def test_status_diposting(self):
        status = statusModel.objects.create(message = 'muke')
        status.save()
        response = Client().get('/')
        self.assertContains(response, 'muke')
    
    def test_views_redirect(self):
        content = {"message" : "muke 123"}
        request = self.client.post('/', data = content)
        self.assertEqual(request.status_code, 302)




class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.driver  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.driver.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_otomatis(self):
        self.driver.get('http://127.0.0.1:8000')
        self.driver.find_element_by_name('message').send_keys("TES INPUT OTOMATIS")
        self.driver.find_element_by_id('submit').click()
        print("Test Completed")

    

    



    
        

    

    


