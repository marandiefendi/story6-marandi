from django import forms
from .models import statusModel

class statusForm(forms.ModelForm):    
    class Meta:
        model = statusModel
        fields = {'message'}
        